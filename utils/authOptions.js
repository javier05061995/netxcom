/** @format */

import CredentialsProvider from "next-auth/providers/credentials";
import User from "@/models/user";
import bcrypt from "bcrypt";
import dbConnect from "./dbConnects";

export const authOptions = {
    session: {
        strategy: "jwt",
    },
    providers: [
        CredentialsProvider({
            async authorize(credentials, req) {
                dbConnect();
                const { email, password } = credentials;
                const user = await User.findOne({ email });
                if (!user) {
                    throw new Error("Invalido Email or Password");
                }
                if (!user?.password) {
                    throw new Error("Please login via the method you used to signup");
                }

                const isPasswordValid = await bcrypt.compare(password, user.password);
                if (!isPasswordValid) {
                    throw new Error("invaildo Email o Password");
                }

                return user;
            },
        }),
    ],
    callbacks: {
        session: async ({ session, token, user }) => {
            session.user = await User.findOne({ email: session.user.email });
            return session;
        },
    },

    secret: process.env.NEXTAUTH_URL,
    pages: {
        signIn: "/login",
    },
};
