/** @format */

import mongoose from "mongoose";
import mongooseUniqueValidator from "mongoose-unique-validator";

const userShema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: [true, "name is required"],
            trim: true,
            unique: true,
            minLength: 1,
            maxLength: 20,
        },
        email: {
            type: String,
            required: [true, "email is required"],
            index: true,
            lowercase: true,
            unique: true,
            trim: true,
            minLength: 5,
            maxLength: 50,
        },
        password: { type: String, required: true },
        role: {
            type: String,
            default: "user", //admin
        },
        image: String,
        resetCode: {
            data: String,
            expiresAt: {
                type: Date,
                default: () => new Date(Date.now() + 10 * 60 * 1000), //10 min,
            },
        },
    },
    { timestamps: true }
);

userShema.plugin(mongooseUniqueValidator);

export default mongoose.models.User || mongoose.model("User", userShema);
