/** @format */

"use client";
import "../app/globals.css";

import Link from "next/link";
import { signOut, useSession } from "next-auth/react";

export default function TopNav() {
    const { data, status, loading } = useSession();

    console.log(data);
    return (
        <nav className="nav shadow p-2 justify-content-between mb-3">
            <Link className="nav-link" href="/">
                🛒 NEXTCOM
            </Link>

            {status === "authenticated" ? (
                <>
                    <Link className="nav-link" href="/dashboard/user">
                        {data?.user?.email}
                    </Link>
                    <a className="nav-link pointer" onClick={() => signOut({ callbackUrl: "/login" })}>
                        Salir
                    </a>
                </>
            ) : status === "loading" ? (
                <a className="nav-link text-danger">Loading</a>
            ) : (
                <div className="d-flex">
                    <Link className="nav-link" href="/login">
                        Login
                    </Link>
                    <Link className="nav-link" href="/register">
                        Register
                    </Link>
                </div>
            )}
        </nav>
    );
}
