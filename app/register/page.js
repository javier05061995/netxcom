/** @format */
"use client";
import { useState } from "react";
import toast from "react-hot-toast";
import { useRouter } from "next/navigation";

export default function Register() {
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [loading, setLoading] = useState(false);

    const router = useRouter();

    const handleSubmit = async e => {
        e.preventDefault();
        console.log(process.env.API);
        try {
            setLoading(true);
            const response = await fetch(`${process.env.API}/register`, {
                method: "POST",
                headers: {
                    "Content-type": "application/json",
                },
                body: JSON.stringify({
                    name,
                    email,
                    password,
                }),
            });
            const data = await response.json();
            if (!response.ok) {
                toast.error(data.error);
                setLoading(false);
            } else {
                toast.success(data.mensaje);
                router.push("/login");
            }

            console.log(name, email, password);
        } catch (error) {
            console.log(error);
            setLoading(false);
        }
    };

    return (
        <main>
            <div className="container0">
                <div className="row d-felx justify-content-center align-items-center vh-100">
                    <div className="col-lg-5 shadow bg-light p-5">
                        <h2 className="mb-4 text-center">Register</h2>
                        <form onSubmit={handleSubmit}>
                            <input
                                type="text"
                                value={name}
                                onChange={e => setName(e.target.value)}
                                className="form-control mb-4"
                                placeholder="indique su name"
                            />
                            <input
                                type="email"
                                value={email}
                                onChange={e => setEmail(e.target.value)}
                                className="form-control mb-4"
                                placeholder="indique su email"
                            />
                            <input
                                type="password"
                                value={password}
                                onChange={e => setPassword(e.target.value)}
                                className="form-control mb-4"
                                placeholder="indique su Contraseña"
                            />

                            <button
                                className="btn btn-primary btn-raised"
                                disabled={loading || !name || !email || !password}
                            >
                                {loading ? "Pof favor Espere" : "Registrar"}
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </main>
    );
}
