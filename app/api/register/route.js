/** @format */

import { NextResponse } from "next/server";
import dbConnect from "@/utils/dbConnects";
import User from "@/models/user";
import bcrypt from "bcrypt";

export async function POST(req) {
    await dbConnect();
    const body = await req.json();
    const { name, email, password } = body;

    try {
        await new User({
            name,
            email,
            password: await bcrypt.hash(password, 10),
        }).save();
        return NextResponse.json({ success: "Registrado exitosamente" });
    } catch (error) {
        console.log(error);
        return NextResponse.json({ error: error.message }, { status: 500 });
    }
}
export async function GET(req) {
    return NextResponse.json({ time: new Date().toLocaleString() });
}
